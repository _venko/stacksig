open System

open Layout

exception StackError of string
exception ParseError of string

/// A single frame in the stack.
type Frame = { tag: int; value: int }
module Frame =
    let make tag value = { tag = tag; value = value }

    let show frame =
        if frame.value = -1 then 
            sprintf "[%i]:<?>" frame.tag
        else 
            sprintf "[%i]:<%i>" frame.tag frame.value

type Value    = int
type Operator = string

/// A side-effect applied to a stack.
type Effect =
    /// Push a literal onto the stack.
    | Literal of Value
    /// Reduce the top two stack elements down to a single element.
    | Reduce of Operator
    /// Duplicate a stack element.
    | Dup
    /// Swap the top two stack elements.
    | Swap
    /// Rotate the top three stack elements.
    | Rot

type Stack = { data: Frame list; lastTag: int }
module Stack =
    let init () = { data = []; lastTag = -1 }

let (|Stack|) (s: Stack) = s.data, s.lastTag

let apply (s: Stack) (e: Effect) =
    match e with
    | Literal value ->
        let tag = s.lastTag + 1
        let newFrame = Frame.make tag value
        { data = newFrame::s.data; lastTag = tag }
    | Reduce operator ->
        match s with
        | Stack (fst::snd::tl, lastTag) ->
            let tag = lastTag + 1
            let tos = Frame.make tag -1
            { data = tos::tl; lastTag = tag }
        | _ -> raise <| StackError "Can't reduce with fewer than 2 items"
    | Dup -> 
        match s with
        | Stack (hd::tl, lastTag) ->
            let tos = Frame.make hd.tag hd.value
            { data = tos::hd::tl; lastTag = lastTag }
        | _ -> raise <| StackError "Can't dup on empty stack"
    | Swap ->
        match s with
        | Stack (fst::snd::tl, lastTag) ->
            { data = snd::fst::tl; lastTag = lastTag }
        | _ -> raise <| StackError "Can't swap with fewer than 2 items"
    | Rot ->
        match s with
        | Stack (a::b::c::tl, lastTag) ->
            { data = c::a::b::tl; lastTag = lastTag }
        | _ -> raise <| StackError "Can't rotate with fewer than 3 items"

let dictionary =
    Map.empty
        .Add("+", Reduce "+")
        .Add("dup", Dup)
        .Add("swap", Swap)
        .Add("rot", Rot)

/// Parses an effect from a word
let parse (word: string) =
    match Int32.TryParse word with
    | true, i -> Literal i
    | _ -> 
        match dictionary.TryFind word with
        | Some effect -> effect
        | _ -> raise << ParseError <| sprintf "Unrecognized word %s" word

let tokenize (str: string) = str.Split " "

// TODO: Figure out diagram entries
type Depth = int
type DiagramEntry = { effect: Effect; stack: Stack; depth: Depth }
module DiagramEntry =
    let make e s d = { effect = e; stack = s; depth = d }
    let show entry = ()

[<EntryPoint>]
let main argv =
    let (<|>) l r = Block.join l r Horizontal
    // Demo of block construction and rendering.
    let block0 = { Block.fromString "AAA\nBBBBB\nCC" with lpad = 4; just = Left }
    let line = Block.fromString "|\n|\n|"
    let block1 = { Block.fromString "AAA\nBBBBB\nCC" with lpad = 4; just = Left }
    printfn "%s" <| Block.render (line <|> block0 <|> line <|> block1 <|> line) 

    // Demo of stack effect tracking
    let mutable stack = Stack.init ()
    printfn "%A\n" stack.data
    "1 2 dup rot +"
    |> tokenize 
    |> Seq.map parse
    |> Seq.iter (fun effect ->
        stack <- apply stack effect
        printfn "%A:\n%A\n" effect <| List.map Frame.show stack.data)
    0