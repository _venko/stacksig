/// A module for performing block-oriented textual layout.
module Layout

open System

let fillChar = "_"

type Justification = Left | Right 

/// Options for joining two blocks of text.
type Join = Vertical | Horizontal

/// A body of text on one or more lines which can be padded to align it with
/// other blocks.
type Block = {
    lpad: int
    rpad: int
    tpad: int
    bpad: int
    just: Justification

    lines: string seq
}

module Block =
    /// An empty block with left-justification.
    let empty () = 
        { lpad = 0; rpad = 0; tpad = 0; bpad = 0; just = Left; lines = Seq.empty }
    
    /// A block with default justification and padding filled with the contents
    /// of the given string.
    /// 
    /// The input string is split along newlines.
    let fromString (str: string) =
        { empty() with lines = str.Split "\n" }

    /// The length of the longest line in a block not including padding.
    let width block =
        block.lines
        |> Seq.map String.length
        |> Seq.max

    /// The length of the longest line in a block including left and right
    /// padding.
    let paddedWidth block = block.lpad + block.rpad + width block

    /// The number of lines in a block.
    let height block = Seq.length block.lines

    /// The number of lines in a block including top and bottom padding.
    let paddedHeight block = block.tpad + block.bpad + height block

    /// Converts the block to a string for display
    let render block =
        let spaces n = String.replicate n fillChar

        let align maxLength line =
            let length = String.length line
            let fillSize = max 0 (maxLength - length)
            let fill = String.replicate fillSize fillChar
            match block.just with
            | Left -> line + fill
            | Right -> fill + line
        let align = align (width block)

        let pad line = (spaces block.lpad) + line + (spaces block.rpad)
        let blankLines len = Seq.replicate len <| spaces (paddedWidth block)

        seq {
            yield! blankLines block.tpad
            yield! block.lines |> Seq.map align |> Seq.map pad
            yield! blankLines block.bpad
        } |> String.concat "\n"


    /// Attaches `blockB` to `blockA` according to the given `joinType`.
    /// 
    /// The resulting block will have the dimensions of the bounding box of the
    /// two boxes.
    /// 
    /// When the join type is `Vertical`, the top of `blockB` will be attached
    /// to `blockA`'s bottom side. That is, `blockB` goes under `blockA`.
    /// 
    /// When the join type is `Horizontal`, the left side of `blockB` will be
    /// attached to `blockA`'s right side. That is, `blockB` goes to the right
    /// of `blockA`.
    let join blockA blockB joinType =
        let joinHorizontal left right =
            let mutable left = left
            let mutable right = right

            let spaces n = String.replicate n fillChar
            let blankLines n = Seq.replicate n <| spaces (paddedWidth left)

            // Take the box with the larger top padding and grow its `lines`
            // upward so that both Blocks end up with the same tpad.
            if left.tpad > right.tpad then
                let diff = left.tpad - right.tpad
                left <- { 
                    left with 
                        tpad = right.tpad
                        lines = seq { yield! blankLines diff; yield! left.lines } }
            elif right.tpad > left.tpad then
                let diff = right.tpad - left.tpad
                right <- { 
                    right with 
                        tpad = left.tpad
                        lines = seq { yield! blankLines diff; yield! right.lines } }

            assert (left.tpad = right.tpad)

            // Take the box with the fewer number of lines and grow its `lines`
            // downward into its bottom padding so that both blocks end up with
            // the same number of lines.
            if Seq.length left.lines > Seq.length right.lines then
                let lineCount = Seq.length right.lines
                let diff = Seq.length left.lines - Seq.length right.lines
                let bpad = max 0 <| right.bpad - Seq.length right.lines
                right <- {
                    left with
                        bpad = bpad
                        lines = seq { yield! right.lines; yield! blankLines diff } }
            elif Seq.length right.lines > Seq.length left.lines then
                let lineCount = Seq.length left.lines
                let diff = Seq.length right.lines - Seq.length left.lines
                let bpad = max 0 <| left.bpad - Seq.length left.lines
                left <- {
                    right with
                        bpad = bpad
                        lines = seq { yield! left.lines; yield! blankLines diff } }

            assert (Seq.length left.lines = Seq.length right.lines)

            // Now take the box with the overall shorter padded height and grow
            // its bottom padding downward so that both boxes have the same
            // overall height.
            if paddedHeight left > paddedHeight right then
                let diff = paddedHeight left - paddedHeight right
                left <- { left with bpad = left.bpad + diff }
            elif paddedHeight right > paddedHeight left then
                let diff = paddedHeight right - paddedHeight left
                right <- { right with bpad = right.bpad + diff }

            assert (paddedHeight left = paddedHeight right)
            
            // Ok so now the heights of the tpads, lines, and bpads should be
            // the same. Next step is to stitch the two blocks together into a
            // new block.
            let leftLines = (render left).Split "\n" |> Seq.ofArray
            let rightLines = (render right).Split "\n" |> Seq.ofArray
            let lines = 
                (leftLines, rightLines)
                ||> Seq.zip
                |> Seq.map (fun (left, right) -> left + right)

            { empty() with lines = lines }

        let joinVertical blockA blockB = failwith "Not implemented"

        match joinType with
        | Horizontal -> joinHorizontal blockA blockB
        | Vertical -> joinVertical blockA blockB